# iMedia24 Coding challenge

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Official Kotlin documentation](https://kotlinlang.org/docs/home.html)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.3/gradle-plugin/reference/html/#build-image)
* [Flyway database migration tool](https://flywaydb.org/documentation/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

### Docker Image
1. Install Docker - you can download it from [Official Docker Website](https://www.docker.com)
2. Verify the version of Docker by running ```$ docker version```
3. Build the application and generate a JAR file of the application using this command `$ ./gradlew build && java -jar build/libs/shop-0.0.1-SNAPSHOT.jar`
4. Create the following Dockerfile in the root of the application

   *Note: Don’t add any extension to dockerfile. Docker recognizes the file by this name only.*
```
   FROM openjdk:8-jdk-alpine 
   ARG JAR_FILE=build/libs/shop-0.0.1-SNAPSHOT.jar 
   COPY ${JAR_FILE} shop-0.0.1-SNAPSHOT.jar 
   ENTRYPOINT ["java","-jar","/shop-0.0.1-SNAPSHOT.jar"]
```
5. Creating Docker Image `$ docker build --build-arg JAR_FILE=build/libs/\*.jar -t shop .`
6. You can check the created image by using the below command `$ docker images`
8. Build a tagged Docker Image with Gradle `$ ./gradlew bootBuildImage --imageName=shop-app`
9. Run the Spring Boot Docker Image by using the following command `$ docker run -p 8090:8080 shop-app`