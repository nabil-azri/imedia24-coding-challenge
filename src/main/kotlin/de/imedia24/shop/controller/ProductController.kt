package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
//import javax.websocket.server.PathParam

@RestController
@RequestMapping("/api")
class ProductController(val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    // Find products by sku
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    // Add a new product
    @PostMapping("/products/add")
    @ResponseStatus(HttpStatus.CREATED)
    fun addProduct(@RequestBody productResponse: ProductResponse): ProductResponse {
        // logger.info("inside addProduct method")
        return productService.addProduct(productResponse)
    }

    // Get product details by list of skus
    @GetMapping
    fun getProductDetailsBySku(@RequestParam("product_sku") productSkus: List<String>) : List<ProductResponse>
        = productService.getProductDetailsBySkus(productSkus)

    @GetMapping("/products")
    fun getAllProducts() : List<ProductResponse> = productService.getAllProducts()

    // Update a product
    @PutMapping("/products/{sku}")
    fun updateProduct(@RequestBody productResponse: ProductResponse,
                        @PathVariable("sku") sku: String) = productService.updateProduct(sku, productResponse)


    // Delete a product
    @DeleteMapping("/products/{sku}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteProduct(@PathVariable("sku") sku: String) = productService.deleteProduct(sku)

}
