package de.imedia24.shop.exception

class ProductNotFoundException(message: String) : RuntimeException(message)
