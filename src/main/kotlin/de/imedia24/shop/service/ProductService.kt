package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.exception.ProductNotFoundException
import mu.KLogging
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(val productRepository: ProductRepository) {

    companion object : KLogging()

    // Find a product by sku
    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    // Create a product
    fun addProduct(productResponse: ProductResponse): ProductResponse {

        val productEntity = productResponse.let {
            ProductEntity(it.sku, it.name, it.description, it.price, it.stock, ZonedDateTime.now(), ZonedDateTime.now())
        }

        productRepository.save(productEntity)

        logger.info("Saved product: $productEntity")

        return productEntity.let {
            ProductResponse(it.sku, it.name, it.description, it.price, it.stock)
        }

    }

    // Get all products
    fun getProductDetailsBySkus(productSkus: List<String>): List<ProductResponse> {
        val products = mutableListOf<ProductResponse>()
        for (p in productSkus){
           val findBySku = productRepository.findBySku(p)
            if (findBySku != null) {
                products.add(findBySku.toProductResponse())
            }
        }
        return products
    }

    // Update a product
    fun updateProduct(sku: String, productResponse: ProductResponse): ProductResponse {
        val existingProduct = productRepository.findById(sku)

        return if (existingProduct.isPresent){
            existingProduct.get()
                .let {
                    it.name = productResponse.name
                    it.description = productResponse.description
                    it.price = productResponse.price
                    productRepository.save(it)
                    ProductResponse(sku, it.name, it.description, it.price, it.stock)
                }
        }else{
            throw ProductNotFoundException("No product found with the id: $sku")
        }
    }

    // Delete a product
    fun deleteProduct(sku: String) {
        val existingProduct = productRepository.findById(sku)

       if (existingProduct.isPresent){
            existingProduct.get()
                .let {
                    productRepository.deleteById(sku)
                }
        }else{
            throw ProductNotFoundException("No product found with the id: $sku")
        }
    }

    fun getAllProducts(): List<ProductResponse> {

        return productRepository.findAll()
            .map {
                ProductResponse(it.sku, it.name, it.description, it.price, it.stock)
            }

    }


}
