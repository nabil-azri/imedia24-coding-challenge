package de.imedia24.shop.controller

import com.ninjasquad.springmockk.MockkBean
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import de.imedia24.shop.util.productResponse
import io.mockk.every
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.reactive.server.WebTestClient
import java.math.BigDecimal
import java.time.ZonedDateTime

@WebMvcTest(controllers = [ProductController::class])
@AutoConfigureWebTestClient
class ProductControllerUnitTest {

    @Autowired
    lateinit var webTestClient: WebTestClient

    @MockkBean
    lateinit var productServiceMock: ProductService


    // Test getProductDetailsBySku() method
    @Test
    fun getProductDetailsBySku() {

        every { productServiceMock.getProductDetailsBySkus(any()) }.returnsMany(
            listOf(
                productResponse("123",
                    "Product 1",
                    "description for product 1",
                    BigDecimal(150.99),
                    100
                ),
                productResponse("456",
                    "Product 2",
                    "description for product 2",
                    BigDecimal(500.99),
                    100
                )
            )
        )

        val productResponses = webTestClient
            .get()
            .uri("/api?product_sku=123, 456")
            .exchange()
            .expectStatus().isOk
            .expectBodyList(ProductResponse::class.java)
            .returnResult()
            .responseBody

        println("productResponses : $productResponses")

        Assertions.assertEquals(2, productResponses!!.size)

    }


    // Test updateProduct() method
    @Test
    fun updateProduct() {

        val updatedProduct = ProductEntity(
            null,
            "Product 1",
            "description for the product 1",
            BigDecimal("150.99"),
            100,
            ZonedDateTime.now(),
            ZonedDateTime.now()
        )

        every { productServiceMock.updateProduct(any(), any()) } returns ProductResponse(
            sku = "234",
            name = "Product 1 updated",
            description = "description for the updated product 1",
            price =  BigDecimal("850.99"),
            stock = 100
        )

        val updatedProductResponse = webTestClient
            .put()
            .uri("/api/products/{sku}", "234")
            .bodyValue(updatedProduct)
            .exchange()
            .expectStatus().isOk
            .expectBody(ProductResponse::class.java)
            .returnResult()
            .responseBody

        Assertions.assertEquals("Product 1 updated", updatedProductResponse?.name)
        Assertions.assertEquals("description for the updated product 1", updatedProductResponse?.description)
        Assertions.assertEquals(BigDecimal("850.99"), updatedProductResponse?.price)

    }

}
