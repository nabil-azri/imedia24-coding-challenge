package de.imedia24.shop.util

import de.imedia24.shop.domain.product.ProductResponse
import java.math.BigDecimal

fun productResponse(
    sku: String? = null,
    name: String = "Product 1",
    description: String = "description for product 1",
    price: BigDecimal = BigDecimal(199.99),
    stock: Int = 100
) = ProductResponse(
    sku,
    name,
    description,
    price,
    stock
)





